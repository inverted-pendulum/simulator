#include <SDL2/SDL.h>
#include <SDL2/SDL_error.h>
#include <SDL2/SDL_ttf.h>
#include <array>
#include <deque>
#include <fmt/core.h>
#include <fmt/ostream.h>
#include <functional>
#include <libserial/SerialPort.h>
#include <vector>
// TODO GCC BUG !!!!!!!!!!!!!!!!!

constexpr static auto fps = 90;
constexpr static auto frame_period = 1000 / fps;
// constexpr static auto msp_port_name = "/dev/ttyACM0";
#define BIT(value, bit) (((uint8_t*)(&value))[bit])

double d_sin(double x) { return sin(x * M_PI / 180); }
double d_cos(double x) { return cos(x * M_PI / 180); }
constexpr static double rad = 180.0 / M_PI;

double sq(double d) { return d * d; }
void update_angle(double& d) { d = std::fmod(d * rad, 360) / rad; }

struct sys {
    sys() { reset(); }
    constexpr static double j_r = 0.0009983;
    constexpr static double l_r = 0.2159;

    constexpr static double j_p = 0.0012;
    constexpr static double l_p = 0.33655;
    constexpr static double m_p = 0.227;

    constexpr static double g = 9.81;

    constexpr static double b_r = 0.0062;
    constexpr static double b_p = 0.0062;

    double th_2;
    double th_1;
    double th_0;

    double al_2;
    double al_1;
    double al_0;

    void reset() {
        th_2 = 0;
        th_1 = 0;
        th_0 = 176 / rad;

        al_2 = 0;
        al_1 = 0;
        al_0 = 120 / rad;
    }

    void update(double tau = 0, double dt = 0.000001) {
        for (int i = 0; i < 4000; ++i) {
            th_0 += dt * th_1;
            th_1 += dt * th_2;

            al_0 += dt * al_1;
            al_1 += dt * al_2;

            update_angle(al_0);
            update_angle(th_0);

            /*
            double F = j_p + 0.25 * m_p * sq(l_p);
            double A =
                j_r + m_p * sq(l_r) + 0.25 * m_p * sq(l_p) * sq(sin(th_0));
            double B = -0.5 * m_p * l_r * l_p * cos(th_0);
            double C = 0.5 * m_p * sq(l_p) * sin(th_0) * cos(th_0);
            double D = 0.5 * m_p * l_r * l_p * sin(th_0);

            double E = -0.5 * m_p * l_r * l_p * cos(th_0);
            double G = -0.25 * m_p * sq(l_p) * sin(th_0) * cos(th_0);
            double H = -0.5 * m_p * g * l_p * sin(th_0);

            double M = tau - b_r * al_1 - al_1 * th_1 * C - sq(th_1) * D;
            double N = -b_p * th_1 - sq(al_1) * G - H;

            fmt::print("A {:0.3f}, B {:0.3f}, M {:0.3f}\n", A, B, M);
            fmt::print("E {:0.3f}, F {:0.3f}, N {:0.3f}\n\n", E, F, N);
            */

            double A = -0.018 - 0.075 * sq(sin(th_0));
            double B = -0.015 * cos(th_0);
            double M = -0.0075 * al_1 * th_1 * sin(2 * th_0) +
                       0.015 * sq(th_1) * sin(th_0) + 0.5 * al_1 - tau;
            M *= -1;
            A *= -1;

            double E = -0.015 * cos(th_0);
            double F = -0.0225;
            double N = 0.00375 * sq(al_1) * sin(2 * th_0) +
                       0.73575 * sin(th_0) + 0.001 * th_1;
            N *= -1;
            E *= -1;

            // fmt::print("A {:0.3f}, B {:0.3f}, M {:0.3f}\n", A, B, M);
            // fmt::print("E {:0.3f}, F {:0.3f}, N {:0.3f}\n\n\n\n", E, F, N);
            //
            // A al + B th = M  // * E
            // E al + F th = N  // * A
            //
            // AE al + BE th = ME
            // AE al + AF th = AN
            // -
            //
            // th (BE - AF) = ME - AN
            // th = (ME - AN) / (BE - AF)
            //
            // al = (M - B th ) / A

            th_2 = (M * E - A * N) / (B * E - A * F);
            al_2 = (M - B * th_2) / A;
        }
    }
};

struct pendulum {
    uint32_t length;
    double angle;
    double angular_velocity;
};

class simulator {
private:
    constexpr static auto thickness = 6;
    constexpr static auto window_name = "simulator";
    constexpr static auto window_width = 1624;
    constexpr static auto window_height = 968;
    constexpr static auto delay_ms = 1;

    constexpr static SDL_Color pendulum_color{255, 0, 0, 0};
    constexpr static SDL_Color carier_color{255, 155, 55, 0};
    constexpr static SDL_Color graph_color{255, 0, 0, 0};
    constexpr static SDL_Color axis_color{0, 105, 0, 0};

    constexpr static auto axis_width = window_width * 14 / 30;
    constexpr static auto axis_height = window_height * 5 / 30;

    constexpr static auto graph_width = window_width * 13 / 30;
    constexpr static auto graph_x = window_width * 14 / 30;

    constexpr static auto carier_y = window_height * 15 / 30;
    constexpr static auto carier_x = window_width * 6 / 30;

    constexpr static auto font_name = "font.ttf";
    constexpr static auto font_size = 64;

    void make_window() {
        window_ = SDL_CreateWindow(window_name, SDL_WINDOWPOS_CENTERED,
                                   SDL_WINDOWPOS_CENTERED, window_width,
                                   window_height, SDL_WINDOW_SHOWN);

        if (window_ == nullptr) {
            fmt::print("{} failed: {}\n", __func__, SDL_GetError());
            exit(EXIT_FAILURE);
        }
    }

    void make_renderer() {
        renderer_ = SDL_CreateRenderer(window_, -1, SDL_RENDERER_ACCELERATED);

        if (renderer_ == nullptr) {
            fmt::print("{} failed: {}\n", __func__, SDL_GetError());
            exit(EXIT_FAILURE);
        }
    }

    void read_font() {
        font_ = TTF_OpenFont(font_name, font_size);
        if (font_ == nullptr) {
            fmt::print("{} failed: {}\n", __func__, TTF_GetError());
            exit(EXIT_FAILURE);
        }
    }

    void update_renderer() {
        if (SDL_GetTicks() - last_frame_timepoint > frame_period) {
            last_frame_timepoint = SDL_GetTicks();
            SDL_RenderPresent(renderer_);
            SDL_RenderClear(renderer_);
        }
    }

    void read_keyboard() {
        SDL_PumpEvents();
        if (keyboard_state_[SDL_SCANCODE_Q]) {
            fmt::print("Quiting...");
            exit(0);
        }

        if (keyboard_state_[SDL_SCANCODE_D]) {
            tau = 0.5;
        }

        if (keyboard_state_[SDL_SCANCODE_A]) {
            tau = -0.5;
        }

        if (keyboard_state_[SDL_SCANCODE_U]) {
            s.th_0 -= 4 / rad;
            SDL_Delay(200);
        }

        if (keyboard_state_[SDL_SCANCODE_O]) {
            s.th_0 += 4 / rad;
            SDL_Delay(200);
        }
    }

    void draw_thick_line(SDL_Point p1, SDL_Point p2, size_t size,
                         SDL_Color color) {
        if (size == 0) {
            return;
        }
        double delta = floor(45 / size);
        if (delta == 0) {
            delta = 1;
        }

        SDL_SetRenderDrawColor(renderer_, color.r, color.g, color.b, color.a);
        for (double a = 0; a <= 360; a += delta) {
            int dx = size * d_cos(a);
            int dy = size * d_sin(a);
            for (auto i : {-1, 0, 1}) {
                SDL_RenderDrawLine(renderer_, p1.x + dx + i, p1.y + dy,
                                   p2.x + dx + i, p2.y + dy);
                SDL_RenderDrawLine(renderer_, p1.x + dx, p1.y + dy + i,
                                   p2.x + dx, p2.y + dy + i);
            }
        }

        SDL_SetRenderDrawColor(renderer_, 0, 0, 0, 0);
    }

    int pct = 10;
    double errored(double d) {
        return d * ((100.0 - pct) - rand() % (2 * pct)) / 100;
    }

    double noise(double d) { return d * (100.0 - rand() % 201) / 100; }

    double max_tau = 2;
    void regulate() {
        /*
        uint32_t r_ang_vel = errored(sys::l_r * s.al_1 * s.al_1);
        uint32_t p_angle = s.th_0 + noise(0.3 / rad);
        double timeout = 1'000;

        // ================================================================== //
        std::string buff;
        fmt::format_to(std::back_inserter(buff), "{} {};", p_angle, r_ang_vel);
        fmt::print("Sent {}\n", buff);
        msp_port.Write(buff);
        msp_port.DrainWriteBuffer();

        // ================================================================== //

        // ================================================================== //
        try {
            msp_port.ReadLine(buff, ';', timeout);
        } catch (...) {
            fmt::print("Read timeout!\n");
            exit(1);
        }

        fmt::print("Got {}\n", buff);
        fmt::print("Expected {}\n", r_ang_vel + p_angle);
        // ================================================================== //

        tau = std::stod(buff);
        */

        const double& x1 = s.th_0 + noise(0.3 / rad);
        const double& x2 = s.al_0;
        const double& x3 = s.th_1;
        const double& x4 = s.al_1;

        double k = 0.2;
        tau = k * (-35.44 * x1 + 0.41 * x2 - 3.32 * x3 + 3.01 * x4);
        if (tau > max_tau) {
            tau = max_tau;
        }
        if (tau < -max_tau) {
            tau = -max_tau;
        }

        double min_tau = max_tau / 30;
        if (tau < min_tau && tau > -min_tau) {
            tau = 0;
        }
    }

    void swing() {
        double min = 155;
        double max = 360 - min;
        if (abs(s.th_0) * rad > min && abs(s.th_0) * rad < max) {
            tau = (s.th_1 > 0) ? -max_tau : max_tau;
        } else if (abs(s.th_0) * rad < min) {
            tau = (s.th_1 > 0) ? max_tau : -max_tau;
        } else if (abs(s.th_0) * rad > max) {
            tau = (s.th_1 > 0) ? max_tau : -max_tau;
        }
    }

    void update_pendulum() {
        tau = 0;
        if (abs(s.th_0 * rad) < 25) {
            regulate();
        } else {
            swing();
        }

        s.update(tau);
        p.angular_velocity = s.th_1 * rad;
        p.angle = -s.th_0 * rad + 180;
        carier_angle_ = -s.al_0 * rad;
        /*
        fmt::print("angle {:0.3f}, angle vel {:0.3f}\n", p.angle,
                   p.angular_velocity);
                   */
    }

    void update_pendulum_axis() {
        pendulum_axis_.x =
            carier_axis_.x + d_sin(carier_angle_) * carier_lengal_;
        pendulum_axis_.y =
            carier_axis_.y + d_cos(carier_angle_) * carier_lengal_ * 0.2;

        pendulum_end_.y =
            pendulum_axis_.y + d_cos(p.angle) * p.length -
            d_sin(carier_angle_) * d_sin(p.angle) * p.length * 0.2;

        pendulum_end_.x =
            pendulum_axis_.x + d_sin(p.angle) * d_cos(carier_angle_) * p.length;
    }

    void draw_pendulum() {
        draw_thick_line(pendulum_axis_, pendulum_end_, thickness,
                        pendulum_color);
    }

    void draw_carier() {
        draw_thick_line(carier_axis_,
                        {carier_axis_.x,
                         carier_axis_.y + static_cast<int>(p.length * 3 / 5)},
                        thickness, carier_color);
        draw_thick_line(carier_axis_, pendulum_axis_, thickness, carier_color);
    }

    std::vector<std::string> strings(const std::vector<int>& values) {
        std::vector<std::string> string_values;
        for (const auto& i : values) {
            string_values.push_back(std::to_string(i));
        }
        return string_values;
    }

    void draw_y_values(int x, int y, const std::vector<std::string>& values,
                       const std::string& y_label) {
        for (size_t i = 0; i < values.size(); ++i) {
            auto surface =
                TTF_RenderUTF8_Blended(font_, values[i].c_str(), graph_color);
            auto texture = SDL_CreateTextureFromSurface(renderer_, surface);
            int height = y + (static_cast<int>(values.size()) / 2 -
                              static_cast<int>(i)) *
                                 2.0 * axis_height / (values.size() - 1);
            SDL_Rect r{x - 50, height, static_cast<int>(values[i].size() * 10),
                       27};
            SDL_RenderCopy(renderer_, texture, nullptr, &r);
            draw_thick_line({x - 1, height}, {x + 1, height}, 1, axis_color);
            SDL_FreeSurface(surface);
            SDL_DestroyTexture(texture);
        }

        auto surface =
            TTF_RenderUTF8_Blended(font_, y_label.c_str(), graph_color);
        auto texture = SDL_CreateTextureFromSurface(renderer_, surface);
        SDL_Rect r{x - 50, y - axis_height - 50,
                   static_cast<int>(y_label.size() * 10), 27};
        SDL_RenderCopy(renderer_, texture, nullptr, &r);
        SDL_FreeSurface(surface);
        SDL_DestroyTexture(texture);
    }

    void draw_x_values(int x, int y) {
        auto step = axis_width / delay_ms;
        for (int i = (-pops) % step; i <= axis_width; i += step) {
            if (i < 0) {
                continue;
            }
            draw_thick_line({x + i, y}, {x + i, y}, 1, axis_color);
        }
        std::string x_label = "time [s]";
        auto surface =
            TTF_RenderUTF8_Blended(font_, x_label.c_str(), axis_color);
        auto texture = SDL_CreateTextureFromSurface(renderer_, surface);
        SDL_Rect r{x + axis_width, y + 50,
                   static_cast<int>(x_label.size() * 10), 27};
        SDL_RenderCopy(renderer_, texture, nullptr, &r);
        SDL_FreeSurface(surface);
        SDL_DestroyTexture(texture);
    }

    void draw_axis(int x, int y, const std::vector<int>& y_values,
                   const std::string& y_label) {
        draw_y_values(x, y, strings(y_values), y_label);
        draw_x_values(x, y);

        SDL_SetRenderDrawColor(renderer_, axis_color.r, axis_color.g,
                               axis_color.b, axis_color.a);
        SDL_RenderDrawLine(renderer_, x, y + axis_height + 20, x,
                           y - axis_height - 20);
        SDL_RenderDrawLine(renderer_, x, y, x + axis_width, y);
        SDL_SetRenderDrawColor(renderer_, 0, 0, 0, 0);
    }

    void draw_graph(const std::deque<double>& data, int x, int y, double max) {
        SDL_SetRenderDrawColor(renderer_, graph_color.r, graph_color.g,
                               graph_color.b, graph_color.a);
        const double scale = axis_height / max;
        for (size_t i = 0; i < data.size() - 1; ++i) {
            for (auto j : {0, 1}) {
                SDL_RenderDrawLine(renderer_, x + i, j + y + scale * data[i],
                                   x + i + 1, j + y + scale * data[i + 1]);
            }
        }
        SDL_SetRenderDrawColor(renderer_, 0, 0, 0, 0);
    }

public:
    simulator() {
        SDL_Init(SDL_INIT_EVERYTHING);
        TTF_Init();
        make_window();
        make_renderer();
        read_font();

        /*
        try {
            msp_port.Open(msp_port_name);
            msp_port.SetBaudRate(LibSerial::BaudRate::BAUD_9600);
            msp_port.SetCharacterSize(LibSerial::CharacterSize::CHAR_SIZE_8);
        } catch (...) {
            fmt::print("Could not find serial port!\n");
            exit(1);
        }
        */
    }

    ~simulator() {
        TTF_CloseFont(font_);
        SDL_DestroyRenderer(renderer_);
        SDL_DestroyWindow(window_);
        TTF_Quit();
        SDL_Quit();
    }

    void run() {
        while (true) {
            update_pendulum();
            read_keyboard();
            update_pendulum_axis();
            if (d_cos(carier_angle_) > 0) {
                draw_carier();
                draw_pendulum();
            } else {
                draw_pendulum();
                draw_carier();
            }

            update_renderer();
            angles.push_back(90 - abs(p.angle));
            if (angles.size() > graph_width) {
                angles.pop_front();
                ++pops;
            }
            draw_axis(graph_x, window_height * 2 / 8, {0, 90, 180}, "|α|[°]");
            draw_graph(angles, graph_x, window_height * 2 / 8, 90);

            angular_velocities.push_back(p.angular_velocity);
            if (angular_velocities.size() > graph_width) {
                angular_velocities.pop_front();
            }

            draw_axis(graph_x, window_height * 6 / 8, {-700, -350, 0, 350, 700},
                      "θ[°/s]");
            draw_graph(angular_velocities, graph_x, window_height * 6 / 8, 700);
            // SDL_Delay(delay_ms);
        }
    }

private:
    SDL_Window* window_{nullptr};
    SDL_Renderer* renderer_{nullptr};
    uint8_t last_frame_timepoint = 0;
    const Uint8* keyboard_state_{SDL_GetKeyboardState(nullptr)};
    SDL_Point pendulum_axis_;
    SDL_Point pendulum_end_;
    pendulum p{.length = window_height / 3,
               .angle = -149.1,
               .angular_velocity = 0};
    uint32_t carier_lengal_ = p.length * 2 / 3;
    double carier_angle_ = 30;
    SDL_Point carier_axis_{carier_x, carier_y};
    std::deque<double> angles;
    std::deque<double> angular_velocities;
    TTF_Font* font_;
    int pops = 0;
    sys s;
    double tau = 0;

    LibSerial::SerialPort msp_port;
};

int main() {
    srand(time(nullptr));
    simulator s;
    s.run();

    return 0;
}
