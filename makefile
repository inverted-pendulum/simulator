PROJ=simulator

all: 
	clang++ -g -Wextra -Wall -O3 -flto -o ${PROJ} main.cpp `sdl2-config --cflags --libs` -lSDL2 -lSDL2_image -lSDL2_ttf -lfmt -lserial

clean:
	rm ${PROJ}
